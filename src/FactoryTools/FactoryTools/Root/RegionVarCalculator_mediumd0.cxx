#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "AsgTools/MessageCheck.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"

//%%%%%%%%
//includes needed for isolation tool
#include "xAODPrimitives/IsolationType.h"
#include "IsolationSelection/IsolationWP.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "PathResolver/PathResolver.h"
//%%%%%%%%

#include <FactoryTools/HelperFunctions.h>
#include <xAODAnaHelpers/HelperFunctions.h>
#include "FactoryTools/RegionVarCalculator_mediumd0.h"
#include "FactoryTools/strongErrorCheck.h"

#include <fstream>

// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_mediumd0)

using namespace asg::msgUserCode;

typedef FactoryTools::HelperFunctions HF;

RegionVarCalculator_mediumd0::RegionVarCalculator_mediumd0()
  : m_iso("CP::IsolationSelectionTool"),
    m_susyTools("ST::SUSYObjDef_xAOD/SUSYTools") {}


//*************************
// D O  I N I T I A L I Z E
//*************************
EL::StatusCode RegionVarCalculator_mediumd0::doInitialize(EL::IWorker * worker) {

  if(m_worker != nullptr){
    std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
    return EL::StatusCode::FAILURE;
  }
  m_worker = worker;

  //isolation tool
  ANA_CHECK( m_iso.setProperty("MuonWP","PflowLoose_VarRad"));
  ANA_CHECK( m_iso.initialize() );

  //cross section tool
  ANA_MSG_DEBUG("Initializing Cross Section Tool");
  ASG_SET_ANA_TOOL_TYPE(m_PMGCrossSectionTool, PMGTools::PMGCrossSectionTool);
  m_PMGCrossSectionTool.setName("myCrossSectionTool");
  m_PMGCrossSectionTool.retrieve();
  //need to give the PMGTool a resolved path, as we can't give it an environmental variable,
  //which is needed to run on the grid
  std::string resolvedPMGfile = PathResolverFindCalibDirectory(m_PMGToolFiles);
  m_PMGCrossSectionTool->readInfosFromDir(resolvedPMGfile);

  //SUSY tools
  xAOD::TEvent* event = m_worker->xaodEvent();
  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);
  bool const isData = !(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ));
  bool const isAtlfast = false;
  ST::ISUSYObjDef_xAODTool::DataSource datasource = (isData ? ST::ISUSYObjDef_xAODTool::Data : (isAtlfast ? ST::ISUSYObjDef_xAODTool::AtlfastII : ST::ISUSYObjDef_xAODTool::FullSim));
  STRONG_CHECK( m_susyTools.setProperty("DataSource", datasource) );
  STRONG_CHECK( m_susyTools.setProperty("ConfigFile", SUSYToolsConfigFileName ) );
  STRONG_CHECK( m_susyTools.retrieve() );

  return EL::StatusCode::SUCCESS;
}


//**************************
// D O   C A L C U L A T E
//**************************
EL::StatusCode RegionVarCalculator_mediumd0::doCalculate(std::map<std::string, anytype>& vars) {

  xAOD::TEvent* event = m_worker->xaodEvent();
  const xAOD::EventInfo* eventInfo = nullptr;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  regionName = eventInfo->auxdecor< std::string >("regionName");

  if(regionName.empty()){
    // If it hasn't been selected in any of the regions from any of the select algs,
    //don't bother calculating anything...
    ANA_MSG_DEBUG("No region name set, no calculations performed.");
    return EL::StatusCode::SUCCESS;
  }
  else if(regionName == "MEDIUMD0"){
    ANA_MSG_DEBUG("Selection region set: MEDIUMD0");
    return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS);
  }
  return EL::StatusCode::SUCCESS;
}


//**************************
// D O   A L L   C A L C
//**************************
EL::StatusCode RegionVarCalculator_mediumd0::doAllCalculations(std::map<std::string, anytype>& vars ) {

  xAOD::TStore * store = m_worker->xaodStore();
  xAOD::TEvent *event = m_worker->xaodEvent();

  auto toGeV = [](double a){return a*.001;};

  const xAOD::EventInfo* eventInfo = nullptr;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = true;
  }

  //Get the pdg Ids for the new particle you want to truth match to
  pdgids = eventInfo->auxdecor< std::string >("BSMpdgids");

  //%%%%%%%%%%%%%%%%%%%%
  // R E C O   L E P T O N S
  //%%%%%%%%%%%%%%%%%%%%

  auto muons_baseline = HF::grabFromStore<xAOD::MuonContainer>("baselineMuons",store);

  float invMass =     -999;
  float deltaRmus =   -999;
  //Get the first two muons from the baseline muon container that was sorted in pt
  //if you have a signal sample with many muons that are not isolated and higher pt
  //than the two isolated muons you are probeing you might need to edit this section
  //so that you loop through the baseline muons checking if they pass the isolation WP
  //and you might want to remove the trigger matching
  //first try seeing how many muons are in the baseline container
  xAOD::Muon* leading_muon = muons_baseline->at(0);
  xAOD::Muon* sub_leading_muon = muons_baseline->at(1);

  fillMuonsVars(leading_muon, "1", vars);
  fillMuonsVars(sub_leading_muon, "2", vars);

  TLorentzVector leading_muon_tlv, sub_leading_muon_tlv;

  leading_muon_tlv.SetPtEtaPhiE(toGeV( leading_muon->pt() ), leading_muon->eta(), leading_muon->phi(), toGeV( leading_muon->e() ) );
  sub_leading_muon_tlv.SetPtEtaPhiE(toGeV( sub_leading_muon->pt() ), sub_leading_muon->eta(), sub_leading_muon->phi(), toGeV( sub_leading_muon->e() ) );

  invMass     = (leading_muon_tlv+sub_leading_muon_tlv).M();
  deltaRmus   = leading_muon_tlv.DeltaR(sub_leading_muon_tlv);

  vars["invMassTwoMuons"] = invMass;
  vars["deltaRTwoMuons"]  = deltaRmus;

  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // S F s  &  W E I G H T S  &  C U T S
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  int bothMusPassTriggerMatching = 0;
  int oppSignMuonEvent = 0;
  int passInvMassCut = 0;
  int bothMuPassIso = 0;
  int BothMuonsPt20 = 0;
  int BothMuonsEta2p5 = 0;
  int BothMuonsMedium = 0;
  float crossSection = 1;
  float kFactor = 1;
  float filterEff = 1;
  float lumi = 1;
  float mcEventWeight = 1;
  float pileupWeight = 1;
  float eventWeight = 1;
  bool passTM_leading_mu = m_susyTools->IsTrigMatched(leading_muon, "HLT_2mu14");
  bool passTM_subleading_mu = m_susyTools->IsTrigMatched(sub_leading_muon, "HLT_2mu14");
  float xsecUncertUp = 1;
  float xsecUncertDown = 1;
  float trigSF = m_susyTools->GetTotalMuonTriggerSF(*muons_baseline,"HLT_2mu14");
  float leading_muonSF = m_susyTools->GetSignalMuonSF(*leading_muon, true, true, true, true);
  float sub_leading_muonSF = m_susyTools->GetSignalMuonSF(*sub_leading_muon, true, true, true, true);
  float totalMuonSF = leading_muonSF * sub_leading_muonSF * trigSF;

  bool doprw = eventInfo->auxdecor< bool >("doprw");
  bool usePMGTool = eventInfo->auxdecor< bool >("usePMGTool");
  datasetid = eventInfo->mcChannelNumber();
  if(usePMGTool){
    kFactor         = m_PMGCrossSectionTool->getKfactor(datasetid);
    filterEff       = m_PMGCrossSectionTool->getFilterEff(datasetid);
    crossSection    = m_PMGCrossSectionTool->getAMIXsection(datasetid);
    xsecUncertUp    = m_PMGCrossSectionTool->getXsectionUncertaintyUP(datasetid);
    xsecUncertDown  = m_PMGCrossSectionTool->getXsectionUncertaintyDOWN(datasetid);
  }
  else{
    kFactor         = eventInfo->auxdecor< float >("kFactor");
    filterEff       = eventInfo->auxdecor< float >("filterEff");
    crossSection    = eventInfo->auxdecor< float >("crossSection");
    xsecUncertUp    = eventInfo->auxdecor< float >("xsecUncertUp");
    xsecUncertDown  = eventInfo->auxdecor< float >("xsecUncertDown");
  }

  if(doprw){
    pileupWeight  = eventInfo->auxdecor< float >("PileupWeight");
    vars["pileupReweightHash"] = static_cast< long long >(eventInfo->auxdecor< ULong64_t >("PRWHash"));	
  }
  else{
    vars["pileupReweightHash"] = -999;
    pileupWeight = 1.;
  }
  vars["pileupWeight"] = pileupWeight;
  vars["mcChannelNumber"] = datasetid;    

  std::string campaign = eventInfo->auxdecor< std::string >("campaign");
  if(campaign == "mc16a"){
    lumi = 36207.66; //pb^-1
  }
  else if(campaign == "mc16d"){
    lumi = 44307.4; //pb^-1
  }
  else if(campaign == "mc16e"){
    lumi = 58450.1; //pb^-1
  }

  vars["totalMuonSF"]        = totalMuonSF;
  vars["totalMuonTriggerSF"] = trigSF;
  vars["muSFMuon1"]          = leading_muonSF;
  vars["muSFMuon2"]          = sub_leading_muonSF;
  vars["xSection"]           = crossSection;
  vars["filterEff"]          = filterEff;
  vars["kFactor"]            = kFactor;
  vars["xSectionUncertUP"]   = xsecUncertUp;
  vars["xSectionUncertDOWN"] = xsecUncertDown;
  vars["lumi"]               = lumi;
  
  //below weights need to be normalised by the total sum of mcEventWeights at AOD level (i.e.
  //no derivation level cuts), so this is effectively the number of weighted events recorded.
  //this is stored in the output file in a histogram MetaData_EventCount, in bin 3,
  //initalSumOfWeights this should be done at plotting stage (so we have the number
  //for the entire sample type).    
  mcEventWeight = eventInfo->auxdecor< float >("mcEventWeight");
  vars["mcEventWeight"]   = mcEventWeight;
  eventWeight = crossSection * filterEff * kFactor * mcEventWeight * pileupWeight * lumi;

  vars["eventWeight"] = eventWeight;
  vars["bothMuEventWeight"]   = eventWeight * totalMuonSF;

  if( passTM_leading_mu==true && passTM_subleading_mu==true ){bothMusPassTriggerMatching=1;}
  if( leading_muon->charge()!=sub_leading_muon->charge() ){oppSignMuonEvent=1;}
  if( m_iso->accept(*leading_muon )==1 && m_iso->accept(*sub_leading_muon)==1 ){
    bothMuPassIso=1;
  }
  if( invMass>110 ){passInvMassCut=1;}
  if( toGeV(leading_muon->pt()) > 20 && toGeV(sub_leading_muon->pt()) > 20){BothMuonsPt20=1;}
  if( leading_muon->eta() < 2.5 && sub_leading_muon->eta() < 2.5){BothMuonsEta2p5=1;}
  if( leading_muon->quality() <= 1 && sub_leading_muon->quality() <= 1){BothMuonsMedium=1;}

  vars["passInvMassCut"] = passInvMassCut;
  vars["bothMusPassTriggerMatching"] = bothMusPassTriggerMatching;
  vars["isOppSignMuonEvent"] = oppSignMuonEvent;
  vars["bothMuPassIso"] = bothMuPassIso;
  vars["BothMuonsPt20"] = BothMuonsPt20;
  vars["BothMuonsEta2p5"] = BothMuonsEta2p5;
  vars["BothMuonsMedium"] = BothMuonsMedium;  
  
  std::vector< std::string > const & passTrigs = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");
  int passHLT_2mu14 = std::find(passTrigs.begin(), passTrigs.end(), "HLT_2mu14") != passTrigs.end();

  vars["passHLT_2mu14"] = passHLT_2mu14;
  
  int passCuts = bothMuPassIso * BothMuonsEta2p5 * BothMuonsMedium * BothMuonsPt20 * passHLT_2mu14 * bothMusPassTriggerMatching * oppSignMuonEvent * passInvMassCut;

  vars["passCuts"]  = passCuts;
  return EL::StatusCode::SUCCESS;
}

void RegionVarCalculator_mediumd0::fillMuonsVars(xAOD::Muon* muon, std::string muonLabel, std::map<std::string, anytype>& vars){

  const xAOD::TrackParticle* muon_track = muon->primaryTrackParticle();
  vars["Muon" + muonLabel + "Charge"]  = muon->charge();
  vars["Muon" + muonLabel + "TrackD0"]  = (muon_track) ? muon_track->d0() : (float)-999;

  double dummy = 0;
  const ElementLink< xAOD::TruthParticleContainer >& muon_truth_tl = muon->auxdata< ElementLink< xAOD::TruthParticleContainer > >("truthParticleLink");
  if(muon_truth_tl.isValid()){
    const xAOD::TruthParticle* muon_truth = *muon_truth_tl;

    if (muon_truth->nParents() >0){      	
      const xAOD::TruthParticle* parent = muon_truth->parent(0);      
      bool matched = isMatched(parent);
      vars["Muon" + muonLabel + "FirstParentTruthMatched"] = matched;
           
      const ElementLink< xAOD::TruthVertexContainer >& parent_truth_tvl = parent->auxdata< ElementLink< xAOD::TruthVertexContainer > >("prodVtxLink");
      if( parent_truth_tvl.isValid()){
	double properLT = tau(parent);
	vars["Muon" + muonLabel + "ParentProperLifetime"] =  properLT;
      }
      else{
	vars["Muon" + muonLabel + "ParentProperLifetime"] =  dummy;
      }
    }
    else{
      vars["Muon" + muonLabel + "ParentProperLifetime"] =  dummy;
      vars["Muon" + muonLabel + "FirstParentTruthMatched"] = false;
    }
  }
  else{
    vars["Muon" + muonLabel + "ParentProperLifetime"] =  dummy;
    vars["Muon" + muonLabel + "FirstParentTruthMatched"] = false;
  } 
}

//lifetime of particle
double RegionVarCalculator_mediumd0::ctau(const xAOD::TruthParticle* particle,
					  const xAOD::TruthVertex *firstvertex) noexcept
{
  using ROOT::Math::XYZVector;
  auto decayVertex = particle->decayVtx();
  XYZVector decay(decayVertex->x(), decayVertex->y(), decayVertex->z());
  XYZVector firstv(firstvertex->x(), firstvertex->y(), firstvertex->z());
  double length = std::sqrt((decay - firstv).Mag2());
  XYZVector p(particle->px(), particle->py(), particle->pz());
  return length * particle->m() / std::sqrt(p.Mag2());
}

double RegionVarCalculator_mediumd0::tau(const xAOD::TruthParticle* particle,
					 const xAOD::TruthVertex *firstvertex) noexcept {
  constexpr double c = 1000./299.792458;
  return c * ctau(particle, firstvertex);
}

double RegionVarCalculator_mediumd0::ctau(const xAOD::TruthParticle* particle) noexcept{

  double CTau = particle->hasProdVtx() ? ctau(particle, particle->prodVtx()) : 0.;
  return CTau;
}

double RegionVarCalculator_mediumd0::tau(const xAOD::TruthParticle* particle) noexcept{

  double Tau = particle->hasProdVtx() ? tau(particle, particle->prodVtx()) : 0.;
  return Tau;
  
}

std::vector<int> RegionVarCalculator_mediumd0::getPdgids(std::string s) {
  std::string delimiter = ",";
  size_t pos_start = 0;
  size_t pos_end = 0;
  size_t delim_len = delimiter.length();
  std::string token;
  std::vector<int> res;

  while ((pos_end = s.find (delimiter, pos_start)) != std::string::npos) {
    token = s.substr (pos_start, pos_end - pos_start);
    pos_start = pos_end + delim_len;
    res.push_back (stoi(token));
  }

  res.push_back (stoi(s.substr (pos_start)));
  return res;
}

bool RegionVarCalculator_mediumd0::isMatched(const xAOD::TruthParticle* parent) {  
  bool matched = false;
  std::vector<int> pdgid_vec = getPdgids(pdgids);     
  for(int pdgid : pdgid_vec){
    if(abs(parent->pdgId())==pdgid){
      matched = true;
    }
  }
  return matched;
}
