#ifndef REGION_VARIABLE_CALCULATOR_MEDIUMD0_H
#define REGION_VARIABLE_CALCULATOR_MEDIUMD0_H

#include "EventLoop/StatusCode.h"
#include "FactoryTools/RegionVarCalculator.h"

#include "AssociationUtils/OverlapLinkHelper.h"
#include <AnaAlgorithm/AnaAlgorithm.h>

#include <AsgTools/AnaToolHandle.h>
#include <IsolationSelection/IIsolationSelectionTool.h>
#include "PMGAnalysisInterfaces/IPMGCrossSectionTool.h"
#include "PMGTools/PMGCrossSectionTool.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/Vertex.h"
#include "Math/Vector3D.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <TH2F.h>

class RegionVarCalculator_mediumd0 : public RegionVarCalculator {

public :
    RegionVarCalculator_mediumd0();

private :
  EL::StatusCode doInitialize(EL::IWorker * worker);
  EL::StatusCode doCalculate        (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doAllCalculations  (std::map<std::string, anytype>& /* vars */ );
   static double ctau(const xAOD::TruthParticle* particle,
		     const xAOD::TruthVertex *firstvertex) noexcept;

  static double tau(const xAOD::TruthParticle* particle,
		    const xAOD::TruthVertex *firstvertex) noexcept;

  static double ctau(const xAOD::TruthParticle* particle)noexcept;

  static double tau(const xAOD::TruthParticle* particle) noexcept;
  std::vector<int> getPdgids(std::string s);
  bool isMatched(const xAOD::TruthParticle* parent);
  void fillMuonsVars(xAOD::Muon* muon, std::string muonLabel, std::map<std::string, anytype>& /* vars */);
  std::string regionName;
  std::string pdgids;

 public :

  // this is needed to distribute the algorithm to the workers
  ClassDef(RegionVarCalculator_mediumd0, 1);
  int datasetid    = -999;
  bool doprw = false;
  bool isMC = false;  

  //const std::string m_PMGToolFiles = "$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/PMGTools/";
  const std::string m_PMGToolFiles = "$PWD/PMG";
  const std::string SUSYToolsConfigFileName = "$FactoryToolsWrapper_DIR/data/FactoryTools/SUSYTools_mediumd0.conf";

  asg::AnaToolHandle<CP::IIsolationSelectionTool> m_iso; //!
  asg::AnaToolHandle<PMGTools::IPMGCrossSectionTool> m_PMGCrossSectionTool; //!
  asg::AnaToolHandle<ST::ISUSYObjDef_xAODTool> m_susyTools;//!
};

#endif //REGION_VARIABLE_CALCULATOR_MEDIUMD0_H
