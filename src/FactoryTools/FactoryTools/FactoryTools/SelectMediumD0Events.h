#ifndef FactoryTools_SelectMediumD0Events_H
#define FactoryTools_SelectMediumD0Events_H

#include <EventLoop/Algorithm.h>
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"

class SelectMediumD0Events : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  //below is set via the running script run_mediumd0.py
  std::string m_campaign;
  bool m_doprw;
  bool m_usePMGTool;
  float m_crossSection;
  float m_kFactor;
  float m_filterEff;
  float m_xsecUncertUp;
  float m_xsecUncertDown;
  std::vector<float> m_pdgids;
  std::string m_BSMpdgids;
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

  // this is a standard constructor
  SelectMediumD0Events ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(SelectMediumD0Events, 1);

  template <class Proxy>
  static bool PtOrder(const Proxy* left, const Proxy* right){ return left->pt() > right->pt() ; }
  std::vector<xAOD::Muon*> sortByPt(xAOD::MuonContainer* inputMuonContainer);
};

#endif
