FROM atlas/analysisbase:21.2.173
ADD . /event-selector
WORKDIR /event-selector

USER root

SHELL ["/bin/bash", "-c"]

#add nobody to the root group

RUN usermod -aG root nobody && \
    chsh -s /bin/bash nobody && \ 
    su nobody && \ 
    id -Gn

RUN source /home/atlas/release_setup.sh &&  \
    chown -R nobody /event-selector && \
    mkdir build run && \
    source src/FactoryTools/FactoryTools/util/dependencyHacks.sh;\
    cd build && \
    cmake ../src/FactoryTools;\
    make;