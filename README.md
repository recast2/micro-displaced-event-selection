This is the event selection code (the "ntuple maker") for the micro-displaced muons analysis (aka medium d0 analysis, or intermediate d0 analysis).

The event selection code used for the actual analysis uses [FactoryTools](https://gitlab.cern.ch/atlas-phys-susy-wg/Common/FactoryTools/), specifically commit 1e5ab428, with the script FactoryTools/FactoryTools/util/run_mediumd0.py. This version here is a "lighter version" intended to help make debugging easier if this code needs to be changed for any reason.

To run the code locally first set up the ATLAS software environment (see later if you don't have access, you can use a Docker image):

```
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
```

NOTE clone with recursive option due to the sub module.

```
git clone --recursive https://gitlab.cern.ch/recast2/micro-displaced-event-selection.git;
cd micro-displaced-event-selection;
mkdir run build;
source src/FactoryTools/FactoryTools/util/dependencyHacks.sh;
```
If asked: "overwrite ‘src/FactoryTools/FactoryTools/dep/xAODAnaHelpers/Root/LinkDef.h’?" press y and enter.

build it:
```
asetup AnalysisBase,21.2.173;
cd build;
cmake ../src/FactoryTools;
make;
source */setup.sh;
```

Then to run it get the input files, the example files to run the original analysis for one point are here /eos/project/r/recast/atlas/ANA-SUSY-2020-09/inputdata/. Otherwise input your own.

You can create a directory with the inputs:
```
mkdir inputdata
mkdir inputdata/mc16a inputdata/mc16d inputdata/mc16e
```

Then if you are using the PMGTool for your cross section, filter efficiency, kfactor, cross section up and down uncertainty tool (see [here](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PMGCrossSectionTool)), making sure that the values in that file are correct for your signal point.

put this into the inputdata directory and name it PMG.txt

then put in your DAODs into the corresponding inputdata/mc16* dir, our DAODs were produced with the SUSY2 derivation with p-tag 4432.

If you are doing pileup reweighting, put your pileup histogram file into the inputdata dir, e.g. inputdata/mc16a_prw.root.

Then run e.g.: 
```
cd run;
run_mediumd0.py --doOverwrite --nevents -1 --campaign mc16a --inputDS ../inputdata/mc16a --doprw 1 --prw_file ../inputdata/mc16a_prw.root  --usePMGTool 0 --crossSection  0.012508 --kFactor 1 --filterEff 1  --xsecUncertUp 0.023 --xsecUncertDown 0.021 --BSMpdgids "1000013,2000013" > log.txt
```
The output will be in the run directory run/submit_dir/hist-{$inputFileName}.root and the tree with the variables in is named tree_MEDIUMD0_.

If you do update the code remember to update the image tag in your steps.yml!!

You can also use the docker image atlas/analysisbase:21.2.173.

```
git clone --recursive https://gitlab.cern.ch/recast2/micro-displaced-event-selection.git;
cd micro-displaced-event-selection;
```
Then make the inputdata dir as above.
```
docker pull atlas/analysisbase:21.2.173;
docker run --rm -it -v $PWD:/micro-displaced-event-selection atlas/analysisbase:21.2.173 bash;
source /release_setup.sh;
cd /micro-displaced-event-selection;
mkdir run build;
cd build;
cmake ../src/FactoryTools;
make;
source */setup.sh;
cd ../run;
```
and run.

micro-displaced-event-selection/src/FactoryTools/FactoryTools/Root/SelectMediumD0Events.cxx selects events that have two muons that pass the baseline muon definition in micro-displaced-event-selection/src/FactoryTools/FactoryTools/data/SUSYTools_mediumd0.conf

Then micro-displaced-event-selection/src/FactoryTools/FactoryTools/Root/RegionVarCalculator_mediumd0.cxx does the work

The running script is here: micro-displaced-event-selection/src/FactoryTools/FactoryTools/util/run_mediumd0.py
